 1. Surface, SurfaceView, TextureView, SurfaceTexture
 Surface is a generic object that can have image buffers drawn into it, which is accepted by a number of Android APIs as a destination for output.

 The camera2 API is one of them, but EGL, MediaCodec, etc, can all use them as well. Concrete endpoints that can accept image buffers can usually be converted to a Surface one way or another.
 These include things like some Android Views, Media classes like MediaRecorder for video encoding, the ImageReader class for efficient CPU access to the underlying graphics buffers, etc.

 SurfaceView is an Android View that represents a hardware overlay surface; it's the most efficient way to draw image buffers to screen. Because it represents a low-level display primitive,
 it's a bit clunky to use; you can get a Surface for it from its SurfaceHolder child object's lifecycle callback.

 TextureView is an Android View that draws image buffers via the GPU. It's more flexible than SurfaceView, but requires GPU composition to operate so it adds a bit of latency and power overhead.
 It can provide a SurfaceTexture to draw into.

 SurfaceTexture is a representation of a GPU texture that can be drawn into. You can use it in EGL to render from via its texture ID, and you can create a Surface from it via
 one of the Surface constructors to give to other APIs to draw into. TextureView uses it since it's a GPU-based View.

URL documents:
#### CAMERA2 ####
https://pierrchen.blogspot.com/2015/01/android-camera2-api-explained.html
https://developer.android.com/reference/android/hardware/camera2/package-summary.html

#### Share screen to PC ####
https://medium.com/swlh/share-android-screen-on-your-system-1052ced277b6

#### Motion layout ####
https://medium.com/google-developers/introduction-to-motionlayout-part-i-29208674b10d (4 parts)
https://proandroiddev.com/complex-ui-animations-on-android-featuring-motionlayout-aa82d83b8660
https://www.youtube.com/watch?v=MiD5sILCDBI&feature=youtu.be&list=PLWz5rJ2EKKc-bcyUTIFAr97ZtRkwM7S4y
