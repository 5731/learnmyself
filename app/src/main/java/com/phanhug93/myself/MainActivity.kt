package com.phanhug93.myself

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.core.app.JobIntentService
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.MutableLiveData
import com.phanhug93.myself.extension.loadImageUrlCenterCrop
import com.phanhug93.myself.service.EvenBusService
import com.phanhug93.myself.ui.OneFragment
import com.phanhug93.myself.ui.TwoFragment
import com.phanhug93.myself.ui.motionLayout.MotionLayoutFragment
import com.phanhug93.myself.ui.thread.WorkerThread
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import timber.log.Timber
import java.util.concurrent.Semaphore

class MainActivity : AppCompatActivity(), LifecycleOwner {
    private val registry = LifecycleRegistry(this)
    private var progress1: Int = 0
    private var step: Int = 0
    private var nunu: MutableLiveData<Int>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        step = 20
        nunu = MutableLiveData<Int>()
        nunu?.value = step
        registry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
        tvPush.setOnClickListener {
            pushEvent()
            Handler(Looper.getMainLooper()).postDelayed({ pushEvent() }, 300L)
        }
        addFragment()
        val data = listOf(1, 2, 4, 5, 6, 7, 8, 9, 10)
        data.filter { it % 2 == 0 }
            .apply { Log.d(MainActivity::class.java.simpleName, this.toString()) }
            .map {
                Log.d(MainActivity::class.java.simpleName, "haha: $it")
            }
        nunu?.observe(this, {
            Log.d(MainActivity::class.java.simpleName, "Nuclear1: ${it}}")
        })
        plus10()
        Log.d(MainActivity::class.java.simpleName, "Nuclear: ${progress1}}")
        imvAvatar.loadImageUrlCenterCrop(url = "https://private-rwd-dev.s3.us-west-2.amazonaws.com/images/197001010900015f3226b22a991.jpg")
    }

    private fun pushEvent() {
        Log.d(MainActivity::class.java.simpleName, "Origin: ${System.currentTimeMillis()}}")
        val intent = Intent(this, EvenBusService::class.java).apply {
            val bundle = Bundle().apply {
                putString(EvenBusService.BUS_DATA_KEY, "event Main")
                putLong(EvenBusService.BUS_DEBOUND_KEY, System.currentTimeMillis())
            }
            action = "HUNG_ACTION"
            putExtras(bundle)
        }
        JobIntentService.enqueueWork(this, EvenBusService::class.java, 5731, intent)
    }

    private fun plus10() {
        val string = """{"eventId":265,"data":{"start_time":"2020-09-25T08:55:00.000000Z"}}"""
        val data = JSONObject(string).getJSONObject("data").getString("start_time")
        Timber.d(data.toString())
        step.let {
            progress1 += it
            nunu?.value = (nunu?.value?.plus(it))
        }
    }

    private fun addFragment() {
        supportFragmentManager.beginTransaction()
            .add(
                R.id.frameContainer,
                MotionLayoutFragment(),
                MotionLayoutFragment::class.java.simpleName
            )
            .commitAllowingStateLoss()
    }

    override fun onStart() {
        super.onStart()
        registry.handleLifecycleEvent(Lifecycle.Event.ON_START)
    }

    override fun onResume() {
        super.onResume()
        registry.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }

    override fun onPause() {
        super.onPause()
        registry.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    }

    override fun onStop() {
        super.onStop()
        registry.handleLifecycleEvent(Lifecycle.Event.ON_STOP)
    }

    override fun onDestroy() {
        super.onDestroy()
        registry.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    }
}