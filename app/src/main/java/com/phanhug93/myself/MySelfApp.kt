package com.phanhug93.myself

import android.app.Application
import timber.log.Timber

class MySelfApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}