package com.phanhug93.myself.extension

import android.net.Uri
import android.view.View
import android.widget.ImageView
import com.phanhug93.myself.ui.glide.GlideApp
import com.phanhug93.myself.ui.glide.ImageViewTarget
import com.phanhug93.myself.ui.glide.providerRequestOptions

fun ImageView.loadImageUrlCenterCrop(
    url: String?,
    placeholderId: Int? = null,
    isWithOutPlaceholder: Boolean = false,
    progressView: View? = null
) {
    GlideApp.with(context).clear(this)
    val requestOptions = providerRequestOptions().apply {
        if (isWithOutPlaceholder) {
            placeholder(null)
        } else {
            placeholderId?.let {
                placeholder(it)
            }
        }
        centerCrop()
    }
    GlideApp.with(context).load(url)
        .apply(requestOptions)
        .thumbnail(0.25f)
        .into(ImageViewTarget(this, progressView))
}

fun ImageView.loadImageUrlCircle(url: String?) {
    GlideApp.with(context).load(url)
        .circleCrop()
        .into(ImageViewTarget(this))
}

fun ImageView.loadImageUriCircle(uri: Uri?) {
    GlideApp.with(context).load(uri)
        .circleCrop()
        .into(ImageViewTarget(this))
}

fun ImageView.loadImageUriCircleV2(url: String?, placeholderId: Int? = null) {
    GlideApp.with(context).clear(this)
    val requestOptions = providerRequestOptions().apply {
        override(720)
        placeholderId?.let {
            placeholder(it)
        }
        circleCrop()
    }
    GlideApp.with(context).load(url)
        .apply(requestOptions)
        .thumbnail(0.25f)
        .into(ImageViewTarget(this))
}