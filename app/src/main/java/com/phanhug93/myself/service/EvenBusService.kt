package com.phanhug93.myself.service

import android.content.Intent
import android.os.SystemClock
import androidx.core.app.JobIntentService
import androidx.lifecycle.MutableLiveData


class EvenBusService : JobIntentService() {
    companion object {
        var BUS = MutableLiveData<CustomEvent>()
        var BUS_DEBOUND_KEY = "minimumIntervalMillis"
        var BUS_DATA_KEY = "data"
    }

    override fun onHandleWork(intent: Intent) {
        //TODO("Not yet implemented")
        // simulate work
        intent.extras?.apply {
            // assuming work is done
            val numVal = getLong(BUS_DEBOUND_KEY, 0L)
            val dataStrVal = getString(BUS_DATA_KEY, "empty")
            SystemClock.sleep(1000)
            if (BUS.hasActiveObservers()) {
                BUS.postValue(CustomEvent(Pair<String, Long>(dataStrVal, numVal)))
            } else {
                // show notification
            }
        }

    }
}

data class CustomEvent(val data: Any)