package com.phanhug93.myself.ui.collapseToolbar

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.phanhug93.myself.R
import kotlinx.android.synthetic.main.activity_collapse_toolbar.*


class CollapsingToolbarActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_collapse_toolbar)
        collapsing_toolbar.setContentScrimColor(Color.RED)
    }
}