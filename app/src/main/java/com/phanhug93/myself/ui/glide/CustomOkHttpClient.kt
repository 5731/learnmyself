package com.phanhug93.myself.ui.glide

import android.annotation.SuppressLint
import com.phanhug93.myself.BuildConfig
import okhttp3.OkHttpClient
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.*
import javax.net.ssl.SSLSocketFactory


class CustomOkHttpClient {
    companion object {
        fun getOkHttpClient(): OkHttpClient.Builder {
            try {
                val trustAllCerts = arrayOf<TrustManager>(providerX509TrustManager())
                val sslSocketFactory = providerSslSocketFactory(trustAllCerts)
                val hostnameVerifier = HostnameVerifier { hostname, _ ->
                    if (BuildConfig.DEBUG) {
                        hostname.toLowerCase(Locale.getDefault()) == "private-rwd-dev.s3.us-west-2.amazonaws.com"
                                || hostname.toLowerCase(Locale.getDefault()) == "1.bp.blogspot.com"
                    } else {
                        hostname.toLowerCase(Locale.getDefault()) == "public-rwd-dev.s3.us-west-2.amazonaws.com"
                    }
                }
                return OkHttpClient.Builder().apply {
                    sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
                    hostnameVerifier(hostnameVerifier)
                }
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }
    }
}

// Create a trust manager that does not validate certificate chains
@SuppressLint("TrustAllX509TrustManager")
fun providerX509TrustManager(): X509TrustManager {
    return object : X509TrustManager {
        @Throws(CertificateException::class)
        override fun checkClientTrusted(
            chain: Array<X509Certificate>?, authType: String?
        ) {
        }

        @Throws(CertificateException::class)
        override fun checkServerTrusted(
            chain: Array<X509Certificate>?, authType: String?
        ) {
        }

        override fun getAcceptedIssuers(): Array<X509Certificate> {
            return arrayOf()
        }
    }
}

// Install the all-trusting trust manager
// Create an ssl socket factory with our all-trusting manager
fun providerSslSocketFactory(trustManagers: Array<TrustManager>): SSLSocketFactory {
    return SSLContext.getInstance("SSL").apply {
        init(null, trustManagers, java.security.SecureRandom())
    }.socketFactory
}