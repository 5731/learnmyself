package com.phanhug93.myself.ui.glide

import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapEncoder
import com.bumptech.glide.request.RequestOptions
import com.phanhug93.myself.R

fun providerRequestOptions() = RequestOptions().apply {
    placeholder(R.drawable.ic_launcher_background)
    error(R.drawable.ic_launcher_background)
    diskCacheStrategy(DiskCacheStrategy.RESOURCE)
    skipMemoryCache(false)
}
