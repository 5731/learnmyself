package com.phanhug93.myself.ui.glide

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.request.target.CustomViewTarget
import com.bumptech.glide.request.transition.Transition

class ImageViewTarget(
    private val imageView: ImageView,
    private var progressView: View? = null
) :
    CustomViewTarget<ImageView, Drawable>(imageView) {
    override fun onLoadFailed(errorDrawable: Drawable?) {
        imageView.background = null
        progressView?.visibility = View.GONE
        imageView.setImageDrawable(errorDrawable)
    }

    override fun onResourceReady(
        resource: Drawable,
        transition: Transition<in Drawable>?
    ) {
        progressView?.visibility = View.GONE
        imageView.background = null
        imageView.setImageDrawable(resource)
    }

    override fun onResourceCleared(placeholder: Drawable?) {
        imageView.background = null
    }

    override fun onStart() {
        super.onStart()
        progressView?.visibility = View.VISIBLE
    }
}