package com.phanhug93.myself.ui.motionLayout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.fragment.app.Fragment
import com.phanhug93.myself.R
import kotlinx.android.synthetic.main.fragment_motion_layout.*
import kotlinx.android.synthetic.main.motion_1_basic.*
import kotlinx.android.synthetic.main.motion_1_basic.motionLayout
import kotlinx.android.synthetic.main.motion_notification.*
import kotlinx.android.synthetic.main.motion_notification.view2
import kotlinx.android.synthetic.main.motion_notification.viewAction
import kotlinx.android.synthetic.main.motion_notification_advance.*
import timber.log.Timber

class MotionLayoutFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.motion_1_basic, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        viewContainer1.setOnClickListener {
//            Timber.d("View1")
//        }
//        view2.setOnClickListener {
//            Timber.d("View2")
//        }
//        viewAction.setOnClickListener {
//            Timber.d("ID start: ${R.id.start2} -- ID end: ${R.id.end2} -- Progress: ${motionLayout.progress}")
//            val progress = motionLayout.progress
//            if (progress <= 0.5f) {
//                motionLayout.transitionToEnd()
//            } else {
//                motionLayout.transitionToStart()
//            }
//        }
    }
}