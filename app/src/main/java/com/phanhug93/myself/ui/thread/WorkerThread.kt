package com.phanhug93.myself.ui.thread

import android.os.Message
import android.util.Log
import java.util.concurrent.Semaphore
import java.util.concurrent.locks.Condition
import java.util.concurrent.locks.ReentrantLock
import java.util.logging.Handler

class WorkerThread(private val semaphores: MutableList<Semaphore>, private val index: Int = 0) :
    Thread() {
    private var condition: Condition? = null
    var progressBarHandler: Handler? = null
    private var progress = 0
    private var isSuspended = false
    private var lock: ReentrantLock? = null

    init {
        lock = ReentrantLock()
        condition = lock?.newCondition()
    }

    override fun run() {
        val currentSemaphore = semaphores[index]
        val nextSemaphore = semaphores[(index + 1) % semaphores.size]

        try {
            while (true) {
                currentSemaphore.acquire()
                lock!!.lock()
                while (isSuspended) {
                    condition!!.await()
                }
                lock!!.unlock()
                sleep(300) // we use a sleep call to mock some lengthy work.
                if (progressBarHandler != null) {
//                    val message: Message = Message.obtain(progressBarHandler)
//                    message.arg1 = 10.let { progress += it; progress }
//                    progressBarHandler.sendMessage(message)
                    Log.d(WorkerThread::class.java.simpleName, "Thread $index : +$progress")
                }
                nextSemaphore.release()
                if (progress == 100) {
                    progress = -10
                }
            }
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }
}