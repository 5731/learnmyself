package com.phanhug93.myself.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.annotation.DimenRes
import androidx.constraintlayout.widget.Placeholder
import androidx.core.content.ContextCompat
import com.phanhug93.myself.R
import com.phanhug93.myself.extension.loadImageUrlCenterCrop
import com.phanhug93.myself.ui.glide.GlideApp
import kotlinx.android.synthetic.main.layout_image_view_corner_radius.view.*

/**
 * Copyright © 2020 Neolab VN.
 * Created by ThuanPx on 2/11/20.
 */
class ImageViewCornerRadius @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_image_view_corner_radius, this, true)

        attrs?.run {
            val typeArray = context.obtainStyledAttributes(attrs, R.styleable.ImageViewCornerRadius)

            val cornerRadius = typeArray.getDimensionPixelSize(
                R.styleable.ImageViewCornerRadius_ivcr_corner_radius,
                0
            )
            if (cornerRadius != 0) {
                cardView.radius = cornerRadius.toFloat()
            }
            val strokeColor =
                typeArray.getColor(R.styleable.ImageViewCornerRadius_ivcr_stroke_color, 0)
            if (strokeColor != 0) {
                cardView.strokeColor = strokeColor
            }
            val strokeWidth = typeArray.getDimensionPixelSize(
                R.styleable.ImageViewCornerRadius_ivcr_stroke_width,
                0
            )
            if (strokeWidth != 0) {
                cardView.strokeWidth = strokeWidth
            }

            typeArray.recycle()
        }
    }


    fun setImageUrlV2(
        url: String?,
        placeholderId: Int? = null,
        isWithoutPlaceholder: Boolean = false
    ) {
        imageView.loadImageUrlCenterCrop(
            url = url,
            placeholderId = placeholderId,
            isWithOutPlaceholder = isWithoutPlaceholder
        )
    }

    fun setImageResource(resourceId: Int) {
        GlideApp.with(context).clear(this)
        GlideApp.with(context)
            .load(ContextCompat.getDrawable(context, resourceId))
            .into(imageView)
    }

    fun setStrokeWidth(@DimenRes resourceId: Int) {
        if (resourceId == 0) {
            cardView.strokeWidth = 0
            return
        }
        cardView.strokeWidth = context.resources.getDimensionPixelOffset(resourceId)
    }

    fun setStrokeColor(colorInt: Int) {
        if (colorInt == 0) {
            cardView.strokeColor = 0
            return
        }
        cardView.strokeColor = ContextCompat.getColor(context, colorInt)
    }
}
